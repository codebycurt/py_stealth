import pygame
import math
import random


pygame.init()

# Define some colors
BLACK    = (   0,   0,   0)
WHITE    = ( 255, 255, 255)
RED      = ( 255,   0,   0)
GREEN    = (   0, 255,   0)
BLUE     = (   0,   0, 255)

size = (800, 600)
screen = pygame.display.set_mode(size)
pygame.display.set_caption("MyPy Game")

clock = pygame.time.Clock()

pi = math.pi


class Player():

    def __init__(self, screen, x, y):
        self.x = x
        self.y = y
        self.width = 20
        self.height = 20
        self.step = 3
        self.screen = screen
        (sw, sh) = self.screen.get_size()
        self.sw = sw
        self.sh = sh

    def move(self, direction):
        if direction == "right" and self.x < self.sw - self.width - 2:
            self.x+=self.step
        if direction == "left" and self.x > 0:
            self.x-=self.step
        if direction == "up" and self.y > 0:
            self.y-=self.step
        if direction == "down" and self.y < self.sh - self.height - 2:
            self.y+=self.step

    def draw(self, screen):
        pygame.draw.rect(screen, GREEN, [self.x, self.y, self.width, self.height], 2)


class Enemy():

    def __init__(self, sw, sh):

        self.color = (random.randint(20,255),random.randint(20,255),random.randint(20,255))
        self.x = random.randint(1,sw)
        self.y = random.randint(1,sh)
        self.radius = 12
        self.step = 1

        self.target_set = False
        self.target_x = -1
        self.target_y = -1

    def set_target(self, sw, sh):
        dist = random.randint(50,150)

        # if near left edge, pick a direction to the right
        if self.x < dist:
            x_target = random.randint(self.x, self.x+dist) + 10
        # if near right edge, pick a direction to the left
        elif sw - self.x < dist:
            x_target = random.randint(self.x-dist, self.x) - 10
        # otherwise, target a random x-coordinate within a specified distance
        else:
            x_target = random.randint(self.x-dist, self.x+dist)
        # if near top, pick a direction below
        if self.y < dist:
            y_target = random.randint(self.y, self.y+dist) + 10
        # if near bottom, pick a direction above
        elif sh - self.y < dist:
            y_target = random.randint(self.y-dist, self.y) - 10
        # otherwise, target a random y-coordinate within the specified distance
        else:
            y_target = random.randint(self.y-dist, self.y+dist)

        self.target_x = x_target
        self.target_y = y_target

        # DEBUG (consoel output)
        print("New Target Set...")
        print("Target X is: " + str(x_target))
        print("Target Y is: " + str(y_target))
        print("--------------------")

    def move(self, sw, sh):

            if not self.target_set:
                self.set_target(sw, sh)
                self.target_set = True

            if self.x > self.target_x:
                self.x-=self.step

            if self.x < self.target_x:
                self.x+=self.step

            if self.y > self.target_y:
                self.y-=self.step

            if self.y < self.target_y:
                self.y+=self.step

            if abs(self.x - self.target_x) < 6 and abs(self.y - self.target_y) < 6:
                print("Target hit!")
                print("+++++++++++")
                self.target_set = False

class object_manager():
    def __init__(self, screen):
        self.enemies = []
        self.screen = screen
        (sw, sh) = self.screen.get_size()
        self.sw = sw
        self.sh = sh

    def create_enemy(self):
        self.enemies.append(Enemy(self.sw, self.sh))

    def update(self):
        for enemy in self.enemies:
            enemy.move(self.sw, self.sh)

    def draw(self):
        for enemy in self.enemies:
            pygame.draw.circle(self.screen, enemy.color, (enemy.x, enemy.y), enemy.radius, 2)

p1 = Player(screen, 50, 50)
om = object_manager(screen)

for num in range(1,10):
    om.create_enemy()

while True:
    # --- Main event loop
    for event in pygame.event.get(): # User did something
        if event.type == pygame.QUIT: # If user clicked close
            pygame.quit()
            quit()

    pressed = pygame.key.get_pressed()

    if pressed[pygame.K_ESCAPE]:
        pygame.quit()
        quit()

    if pressed[pygame.K_d]:
        p1.move("right")
    if pressed[pygame.K_a]:
        p1.move("left")
    if pressed[pygame.K_w]:
        p1.move("up")
    if pressed[pygame.K_s]:
        p1.move("down")

    screen.fill(BLACK)

    p1.draw(screen)
    om.update()
    om.draw()

    pygame.display.flip()
    clock.tick(60)

pygame.quit()
quit()